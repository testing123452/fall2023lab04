package debugging.assignment;

public class Company {
    private Employee[] employees;

    public Company(Employee[] employees) {
      this.employees = new Employee[employees.length];

        for(int i = 0; i < employees.length; i++) {
            this.employees[i] = new Employee(employees[i]);
        }  
    }    

    public Company(Company other) {
        this(other.employees);
    } 

    /**
     * Exercise: Fill in the JavaDocs comment to describe what this method is supposed to do!
     * @param name
     * @return true or false if a person (based on their name) is an employee who works for the company.
     */
    public boolean doesWorkFor(String name) {
        for (Employee e : employees) {
            if (e.getName().equals(name)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Company)) {
            return false;
        }

        Company other = (Company) o;
        
    /*  System.out.println(this.employees[0].getName());
        System.out.println(this.employees[1].getName());
        System.out.println(this.employees[2].getName());
        System.out.println(this.employees[0].getEmployeeId());
        System.out.println(this.employees[1].getEmployeeId());
        System.out.println(this.employees[2].getEmployeeId());
        System.out.println(other.employees[0].getName());
        System.out.println(other.employees[1].getName());
        System.out.println(other.employees[2].getName());
        System.out.println(other.employees[0].getEmployeeId());
        System.out.println(other.employees[1].getEmployeeId());
        System.out.println(other.employees[2].getEmployeeId()); */

        for (int i = 0; i < other.employees.length; i++) {
            if (!other.employees[i].equals(this.employees[i])) {
                return false;
            }
        }

        // temporary print statement to demonstrate how you can see print statements within a unit test.
        // System.out.println("hello world");

        return true;
    }

}
